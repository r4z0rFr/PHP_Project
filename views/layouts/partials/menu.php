<ul class="list-group">
  <li class="list-group-item d-flex justify-content-between align-items-center">
    Liens
    <span class="badge badge-primary badge-pill"><?= count($aLinks[0]) ?></span>
  </li>
  <li class="list-group-item d-flex justify-content-between align-items-center">
    Images
    <span class="badge badge-primary badge-pill"><?= count($imgLinks[0]) ?></span>
  </li>
</ul>


