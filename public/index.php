<?php

const PREG_MUSTACHE = "/{{ ([a-z]*)\('([A-Za-z _éèàâêç!?.]*)'\) }}/";

$indexes = [
    'index',
    'contact'
];

$files = init($indexes);

$views = build($files);

switch ($_GET['p']) {
    case 'contact':
        return view($views, 'contact', $indexes);
        break;

    default:
        return view($views, 'index', $indexes);
        break;
}



/**
 * init the views
 * @return void
 */
function init($views)
{
    $initViews = [];

    foreach ($views as $view) {
        array_push($initViews, ['name' => $view, 'view' => file_get_contents(pathReplace($view))]);
    }

    return $initViews;
}

/**
 * return the specified view
 * @return View show the wanted view
 */
function view($view, $name, $indexes)
{
    echo $view[array_keys($indexes, $name)[0]][$name];
}

/**
 * replace . with /
 * @param  string $path the path of the view
 * @return string 		the replaced path
 */
function pathReplace($path)
{
    return '../views/' . str_replace('.', '/', $path) . '.coke.php';
}
/**
 * check for env template
 * @param  string $view The View
 * @return void
 */
function build($views)
{
    $buildedViews = [];

    foreach ($views as $view) {
        preg_match_all(PREG_MUSTACHE, $view['view'], $finded, PREG_SET_ORDER);

        $template = checkForParam($finded, $view['view']);

        array_push($buildedViews, [$view['name'] => $template]) ;
    }
    return $buildedViews;
}

/**
 * Check for param and modify it
 * @param  array $dbMustache list of all finded mustaches
 * @return void
 */
function checkForParam($dbMustache, $view)
{
    foreach ($dbMustache as $mustache) {
        $transformed = $mustache[1]($mustache[2], $mustache[1]);

        $view = preg_replace($transformed[1], $transformed[0], $view);
    }
    return $view;
}

/**
 * get the value of the specified env
 * @param  string $name name of param
 * @return string name of $name
 */
function env($name, $type)
{
    $config = file_get_contents('../config/config.env');

    $preg = '/(?<='.$name.'\=)[a-zA-Z0-9- ]*/';

    preg_match($preg, $config, $find);

    $regex = createRegex($name, $type);

    return [$find[0], $regex];
}

/**
 * Return a title
 * @param  string $name The text of the title
 * @param  string $type the name of the function
 * @return array       the html balise and the full mustache string ({{ title('lorem') }})
 */
function title($name, $type)
{
    $regex = createRegex($name, $type);

    return ["<h1>$name</h1>" , $regex];
}

/**
 * generate a <p> balise with text
 * @param  string $name The text
 * @param  string $type the name of the function
 * @return array       the html balise and the full mustache string ({{ title('lorem') }})
 */
function content($name, $type)
{
    $regex = createRegex($name, $type);

    return ["<p class='lead'>$name</p>" , $regex];
}

/**
 * genarate a <li>
 * @param  string $name The text
 * @param  string $type the name of the function
 * @return array       the html balise and the full mustache string ({{ title('lorem') }})
 */
function navlink($name, $type)
{
    $regex = createRegex($name, $type);

    return ["<li class='nav-item'>
            	<a class='nav-link' href='#''>$name<span class='sr-only'></span></a>
          	</li>" , $regex];
}

/**
 * Create a regex for the specified function to find it on the html template
 * @param  [type] $name [description]
 * @param  [type] $type [description]
 * @return [type]       [description]
 */
function createRegex($name, $type)
{
    return "/{{ $type\('$name'\) }}/";
}

function array_flatten($array, $keys)
{
    $i = 0;

    $array = [];

    foreach ($array as $key) {
    }
}
